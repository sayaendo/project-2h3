﻿using UnityEngine;
using System.Collections;

public class Level : MonoBehaviour {

	static Level manager;
	
	bool applicationIsQuitting;

	public int nextLevelIndex;
	
	public static Level Manager {
		get {
			if (manager.applicationIsQuitting) {
				return null;
			} else {
				return manager;
			}
		}
	}

	public bool lastLevel = false;

	public GameObject[] phases;

	int currentPhaseIndex;
	GameObject currentPhase;

    Gameflow gameflow;

	// Use this for initialization
	void Start () {

		manager = this;

		currentPhase = Instantiate (phases [0]) as GameObject;

        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();

    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnDestroy() {
		applicationIsQuitting = true;
	}

	public void AdvancePhase() {
		Destroy (currentPhase);

		if (++currentPhaseIndex != phases.Length) {
			currentPhase = Instantiate (phases [currentPhaseIndex]) as GameObject;
		} else {
			Invoke ("NextLevel", 7f);
			// END THE LEVEL
		}
	}

	public void NotifyDeath(Vector3 pos) {
		currentPhase.GetComponent<Phase> ().NotifyDeath ();
	}

	void NextLevel() {
		PlayerPrefs.SetInt ("Lives", gameflow.CurrentLives);
		PlayerPrefs.SetInt ("Score", gameflow.CurrentScore);
		PlayerPrefs.SetInt ("Credits", gameflow.CurrentCredits);
		PlayerPrefs.SetFloat ("Power", Ship.Instance.power);

		if (lastLevel) {
			float multiplier;

			if (PlayerPrefs.GetString ("Mode") != "NORMAL") {
				multiplier = 340f / Time.timeSinceLevelLoad + 1f;
			} else {
				multiplier = 240f / Time.timeSinceLevelLoad + 1f;
			}

			PlayerPrefs.SetFloat ("TimeMultiplier", multiplier);

			if (gameflow.CC1) {
				PlayerPrefs.SetString ("Clear Code", "1CC");
				multiplier *= 2;
				PlayerPrefs.SetFloat ("CreditMultiplier", 2f);
			} else {
				PlayerPrefs.SetString ("Clear Code", "CLEAR");
				multiplier *= 1.2f;
				PlayerPrefs.SetFloat ("CreditMultiplier", 1.2f);
			}

			int score = gameflow.CurrentScore * (int) multiplier;

			PlayerPrefs.SetInt ("Best Score", score);

			PlayerPrefs.Save ();

			UnityEngine.SceneManagement.SceneManager.LoadScene (5);
		} else
			UnityEngine.SceneManagement.SceneManager.LoadScene (nextLevelIndex);
	}


}
