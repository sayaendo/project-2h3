﻿using UnityEngine;
using System.Collections.Generic;

public class StartGame : MonoBehaviour {

	public int NormalIndex;
	public int HardIndex;

	public bool clearPlayerPrefs;

	public enum Menu { NORMAL, HARD };

	Menu mode;

	GameObject arrow;
	GameObject normal;
	GameObject hard;

	Vector3 normalPos;
	Vector3 hardPos;

	// Use this for initialization
	void Start () {

		if (clearPlayerPrefs)
			PlayerPrefs.DeleteAll ();

		arrow = GameObject.FindGameObjectWithTag ("Arrow");
		normal = GameObject.FindGameObjectWithTag ("Powerup");
		hard = GameObject.FindGameObjectWithTag ("Scoreup");


		RectTransform rect = normal.GetComponent<RectTransform> ();
		RectTransform rect2 = hard.GetComponent<RectTransform> ();

		normalPos = rect.TransformPoint(rect.rect.center + 0.5f * new Vector2(-rect.rect.width, 0));
		hardPos = rect2.TransformPoint (rect2.rect.center+ 0.5f * new Vector2(-rect2.rect.width, 0));
		arrow.transform.position = normalPos;
	}

	
	// Update is called once per frame
	void Update () {

		if ((Input.GetButtonDown ("Fire"))
			|| (Input.GetButtonDown("Pause"))
			|| (Input.GetButtonDown("Focus"))) {

			PlayerPrefs.SetInt ("Lives", 3);
			PlayerPrefs.SetInt ("Score", 0);
			PlayerPrefs.SetInt ("Credits", 1);
			PlayerPrefs.SetFloat ("Power", 0);
			PlayerPrefs.SetInt ("Best Score", 0);
			PlayerPrefs.SetFloat ("TimeMultiplier", 1f);
			PlayerPrefs.SetFloat ("CreditMultiplier", 1f);

			PlayerPrefs.SetString ("Mode", mode.ToString());

			PlayerPrefs.Save ();

			if (mode == Menu.NORMAL) {
				UnityEngine.SceneManagement.SceneManager.LoadScene (NormalIndex);
			} else {
				UnityEngine.SceneManagement.SceneManager.LoadScene (HardIndex);
			}
		}

		if (Input.GetButtonDown("Up") || Input.GetButtonDown("Down")) {
			mode = (Menu) (((int) (mode + 1)) % 2);

			if (mode == Menu.NORMAL)
				arrow.transform.position = normalPos;
			else
				arrow.transform.position = hardPos;
		}
	
	}
}
