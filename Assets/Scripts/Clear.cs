﻿using UnityEngine;
using System.Collections;

public class Clear : MonoBehaviour {

    Gameflow gameflow;

	// Use this for initialization
	void Start () {
		iTween.ScaleTo (gameObject, iTween.Hash("scale", new Vector3 (200, 200), "time", 2.5f, "oncomplete", "Destroy",
		                                        "easetype", "easeInSine"));
		iTween.FadeTo (gameObject, iTween.Hash ("alpha", 0, "time", 3));

        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "EnemyBullet") {
			col.gameObject.SetActive (false);
			gameflow.AddScore(10, col.gameObject.transform.position);
		}

		if (col.gameObject.tag == "Enemy") {
			col.gameObject.SetActive (false);
			gameflow.AddScore(200, col.gameObject.transform.position);
		}
	}

	void Destroy() {
		Destroy (gameObject);
	}
}
