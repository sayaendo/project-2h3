﻿using UnityEngine;
using System.Collections;

public class BulletPool : MonoBehaviour {

    /// <summary>
    /// The prefab that this pool uses
    /// </summary>
	public GameObject bullet;

	Pool pool;
    
	void Start () {
		pool = new Pool (bullet, transform, 50, true);
	}

	public GameObject GetPooled() {
		return pool.GetPooled ();
	}
}
