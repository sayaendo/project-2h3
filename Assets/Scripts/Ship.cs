﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	static Ship instance;
	
	bool applicationIsQuitting;
	
	public static Ship Instance {
		get {
			if (instance.applicationIsQuitting) {
				return null;
			} else {
				return instance;
			}
		}
	}

	public GameObject explosion;

	public float power;

	public float movementSpeed;

	bool flickering;
	
	Rigidbody2D rb;

	SpriteRenderer sprite;

	public GameObject[] gunPrefabs;
	GameObject mainGun;
	Gun[] guns;

	public int gunCode; //public is temp, to be taken from player prefs

	Vector3 spawnPoint;

    Gameflow gameflow;

	public Vector3 SpawnPoint {
		get {
			return spawnPoint;
		}
	}

    SoundManager sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        instance = this;

        sprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();

        CreateGun(gunCode);
        power = PlayerPrefs.GetFloat("Power");
        Powerup(0);
        spawnPoint = transform.position;

        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();
    }
    

	void Update () {

		Vector2 vel = new Vector2 (movementSpeed * Input.GetAxis ("Horizontal"), movementSpeed * Input.GetAxis ("Vertical"));

		if (Input.GetButton("Focus")) {
			vel *= 0.4f;
		}

		rb.velocity = vel;
	}

	bool shootSound;

	void FixedUpdate() {
		if (Input.GetButton ("Fire")) {
			foreach (Gun g in guns) {
				g.Shoot();
			}
//			SoundManager.Manager.PlaySfx(SoundManager.SFX.PlayerShoot);
		}	
	}

	public bool invulnerable;

	void OnTriggerEnter2D(Collider2D other) {
		if (!invulnerable)
			if (other.gameObject.tag == "EnemyBullet" || other.gameObject.tag == "Enemy") {
				sound.PlaySfx(SoundManager.SFX.Explosion);
				Instantiate (explosion, transform.position, Quaternion.identity);
				gameflow.Death ();
			}
	}

	public void Powerup(float upgradeAmount) {
		power = Mathf.Clamp(power+upgradeAmount, 0, gunPrefabs.Length - 1); // 5 powerups per gun change
		if ((int) power > gunCode) {
			CreateGun((int) power);
			sound.PlaySfx(SoundManager.SFX.Powerup);
		}
	}

	void OnEnable() {
		StartCoroutine ("Flicker");
	}

	IEnumerator Flicker() {
		for (int i = 0; i < 15; i++) {
			yield return new WaitForSeconds(0.1f);
			sprite.enabled = !sprite.enabled;
		}
		sprite.enabled = true;
	}

	void CreateGun(int code) {
		gunCode = code; 
		if (mainGun != null) {
			Destroy (mainGun);
		}
		mainGun = Instantiate (gunPrefabs [gunCode], transform.position, Quaternion.identity) as GameObject;
		mainGun.transform.parent = transform;
		guns = mainGun.GetComponents <Gun> ();
	}
	
	void OnDestroy() {
		applicationIsQuitting = true;
	}

}
