﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour {

    Gameflow gameflow;

    SoundManager sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "Powerup") {
			Ship.Instance.Powerup (0.13f);
            gameflow.AddScore(10, transform.position);
			sound.PlaySfx(SoundManager.SFX.Pickup);
			Destroy (other.gameObject);
		} else if (other.gameObject.tag == "Scoreup") {
            gameflow.AddScore(100, transform.position);
			sound.PlaySfx(SoundManager.SFX.Pickup);
			Destroy (other.gameObject);
		}
	}
}
