﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {
    Gameflow gameflow;

    void Start()
    {
        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();

        GetComponent<MeshRenderer> ().sortingLayerName = "HUD";
		GetComponent<MeshRenderer> ().sortingOrder = 1;
	}
	
	void Update () {
		GetComponent<TextMesh> ().text = "Score: " + gameflow.CurrentScore;
	}
}
