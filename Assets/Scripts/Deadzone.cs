﻿using UnityEngine;
using System.Collections;

public class Deadzone : MonoBehaviour {

	public bool includePowerup;

	public bool includeEnemy;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D col) {

		if (col.gameObject.tag == "ShipBullet" || col.gameObject.tag == "EnemyBullet") {
			col.gameObject.SetActive (false);
		}

		if (includePowerup && (col.gameObject.tag == "Powerup" || col.gameObject.tag == "Scoreup")) {
			Destroy (col.gameObject);
//			col.gameObject.SetActive(false);
		}

		if (includeEnemy && (col.gameObject.tag == "Enemy")) {
			Destroy (col.gameObject);
		}

		//Destroy (col.gameObject);
	} 
}
