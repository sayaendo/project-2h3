﻿using UnityEngine;
using System.Collections;

public interface EnemyAI {

	void Activate();

	void Deactivate();

}
