﻿using UnityEngine;
using System.Collections;

public class RedExplosion : MonoBehaviour {

	// Use this for initialization
	void Start () {

		iTween.ScaleTo (this.gameObject, iTween.Hash ("x", transform.localScale.x * 0.6,
		                                            "y", transform.localScale.y * 0.6,
		                                            "easetype", "linear",
		                                            "oncomplete", "End", "oncompletetarget", gameObject));
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void End () {
		Destroy (gameObject);
	}
}
