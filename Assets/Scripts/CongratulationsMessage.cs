﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CongratulationsMessage : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Text txt = gameObject.GetComponent<Text> ();

		Text txt2 = GameObject.FindGameObjectWithTag("Level").GetComponent<Text> ();

		string code = PlayerPrefs.GetString ("Clear Code");
		string mode = PlayerPrefs.GetString ("Mode");

		if (code == "CLEAR" && mode == "NORMAL") {
			txt.text = "Try to finish without continuing.";
		} else if (mode == "NORMAL") {
			txt.text = "Try a higher difficulty.";
		} else {
			txt.text = "You're the best!";
		}

		txt2.text = "Time Multiplier: " + PlayerPrefs.GetFloat ("TimeMultiplier") + "\n";
		txt2.text += "Clear Multiplier: " + PlayerPrefs.GetFloat ("CreditMultiplier");


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
