﻿using UnityEngine;
using System.Collections;

public class GameOverDeath : MonoBehaviour {

	public int LoadSceneIndexOnClick;

	void Update () {

		if ((Input.GetButtonDown ("Fire"))
			|| (Input.GetButtonDown("Pause"))
			|| (Input.GetButtonDown("Focus"))) {

			UnityEngine.SceneManagement.SceneManager.LoadScene (LoadSceneIndexOnClick);
		}


	}
}
