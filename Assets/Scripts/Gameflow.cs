﻿using UnityEngine;
using System.Collections;

public class Gameflow : MonoBehaviour {

	public GameObject popup;

	int currentLives;

	public int CurrentLives {
		get {
			return currentLives;
		}
	}

	int currentScore;

	public int CurrentScore {
		get {
			return currentScore;
		}
	}

	bool gameOvering;

	public bool GameOvering {
		get { 
			return gameOvering;
		}
	}

	int currentCredits;


	public int CurrentCredits {
		get {
			return currentCredits;
		}
	}

	int initialLives;

	public float time;


	public bool debug;

	void Awake () {
		currentLives = PlayerPrefs.GetInt ("Lives");
		initialLives = currentLives;
		currentScore = PlayerPrefs.GetInt ("Score");
		currentCredits = PlayerPrefs.GetInt ("Credits");
	}

	void Update () {
		time = Time.timeSinceLevelLoad;
	}

	bool cc1 = true;

	public bool CC1 {
		get { 
			return cc1;
		}
	}

	public void Death() {
		Ship.Instance.gameObject.SetActive (false);
		if (!debug)
			currentLives--;

		if (currentLives == -1) {
			currentLives = 0;
			if (currentCredits == 0) {

				if (PlayerPrefs.GetInt ("Best Score") < currentScore) {
					PlayerPrefs.SetInt ("Best Score", currentScore);
					PlayerPrefs.Save ();
				}
				
				gameOvering = true;
				Invoke ("GameOver", 2f);
			
			} else {
				currentLives = initialLives;

				if (PlayerPrefs.GetInt ("Best Score") < currentScore) {
					PlayerPrefs.SetInt ("Best Score", currentScore);
					PlayerPrefs.Save ();
				}

				cc1 = false;

				currentScore = 0;
				currentCredits--;

				StartCoroutine ("Respawn", 2);
			}
		} else 
			StartCoroutine ("Respawn", 2);
	}

	IEnumerator Respawn() {
		Ship ship = Ship.Instance;
		// disable collider, itween to position, enable collider after blinking sprite for a couple seconds?
		yield return new WaitForSeconds(1.5f);
		ship.transform.position = Ship.Instance.SpawnPoint;
		ship.invulnerable = true;
		ship.gameObject.SetActive (true);

		yield return new WaitForSeconds(2f);
		ship.invulnerable = false;
	}
	
	/// <param name="points">Points added.</param>
	public void AddScore(int points) {
		currentScore += points;
	}

	/// <summary>
	/// Adds the points, and also displays a popup of the points added.
	/// </summary>
	/// <param name="points">Points added.</param>
	/// <param name="pos">Position of the popup.</param>
	public void AddScore(int points, Vector3 pos) {
		AddScore (points);
		TextMesh mesh = (Instantiate (popup, pos, Quaternion.identity) as GameObject).GetComponent<TextMesh> ();
		mesh.text = points.ToString ();
	}

	public void GameOver() {
		if (PlayerPrefs.GetInt ("Best Score") < currentScore) {
			PlayerPrefs.SetInt ("Best Score", currentScore);
			PlayerPrefs.Save ();
		}
		PlayerPrefs.SetString ("Clear Code", "DEATH");
		PlayerPrefs.Save ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (3);
	}
    
}
