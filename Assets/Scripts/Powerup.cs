﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

	Rigidbody2D rb;

	Vector2 vel;

	bool down;

	// Use this for initialization
	void Start () {

		rb = GetComponent<Rigidbody2D> ();

		iTween.MoveAdd (gameObject, iTween.Hash ("y", 4, "time", 1, "oncomplete", "Down"));
	
	}

	void Down() {
//		iTween.MoveTo (gameObject, iTween.Hash ("y", -11, "time", 5, "easetype", "easeInQuid"));
		down = true;
		rb.velocity = new Vector2 (0, -0.1f);
	}

	void Awake() {

	}
	
	// Update is called once per frame
	void Update () {
		if (down) {
			rb.velocity = rb.velocity * 1.035f;
		}
//		GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 5));
//		rb.velocity = rb.velocity * 0.95f;
	}
}
