﻿using UnityEngine;
using System.Collections;

public class Phase : MonoBehaviour {

	public int waitFor;
	public enum WaitType { Seconds, DeathNotifications };
	public WaitType waitForType;
	public GameObject[] enemies;
	public float[] enemyEntraceTimes;

	int deathAmount;

	void Start () {

		for (int i = 0; i < enemies.Length; i++) {
			StartCoroutine(Spawn (enemies[i], enemyEntraceTimes[i]));
		}

		if (waitForType == WaitType.Seconds) {
			Invoke("EndPhase", waitFor);
		}
	
	}

	IEnumerator Spawn(GameObject enemy, float time) {
		yield return new WaitForSeconds(time);
		GameObject e = Instantiate (enemy) as GameObject;
		e.transform.parent = transform;
	}

	public void NotifyDeath() {
		deathAmount++;
		if (waitForType == WaitType.DeathNotifications && deathAmount == waitFor) {
			EndPhase ();
		}
	}

	void EndPhase() {
		Level.Manager.AdvancePhase ();
	}
}
