﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	Animator animator;
    
	void Start () {
    	animator = GetComponent<Animator> ();
	}
	
	void Update () {
		if (animator.GetCurrentAnimatorStateInfo (0).IsName ("NewState"))
			Destroy (gameObject);
	}



}
