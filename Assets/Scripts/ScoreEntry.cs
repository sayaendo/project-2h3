﻿using System;
using System.Text;

public class ScoreEntry : IComparable {

	int score;
	string info;
	string time;

	public int Score {
		get {
			return score;
		}
	}

	public string Info {
		get { 
			return info;
		}
	}

	public string Time {
		get { 
			return time;
		}
	}

	public ScoreEntry(string info, int score, string time) {
		this.info = info;
		this.score = score;
		this.time = time;
	}

	public int CompareTo(object other) {
		if (other == null)
			return 1;

		if (other.GetType() == this.GetType()) {
			ScoreEntry otherEntry = other as ScoreEntry;

			return this.Score - otherEntry.Score;
		} else {
			return 1;
		}
	}

	public override string ToString() {
		StringBuilder builder = new StringBuilder();

		if (Info == "1CC" || Info == "N/A")
			builder.Append (Info + "  ").Append ("                ");
		else
			builder.Append (Info).Append ("                ");

		if (Time == "N/A")
			builder.Append (Time + "          ");
		else
			builder.Append (Time);

		builder.Append ("        ").Append ((Score < 0) ? (0) : (Score));

		return builder.ToString();
	}

}
