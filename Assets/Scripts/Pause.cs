﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Pause : MonoBehaviour {

	public GameObject UI;

	bool paused;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetButtonDown ("Pause")) {
			PauseGame ();
		}

		if (paused && Input.GetKeyDown (KeyCode.X)) {
			RestartGame ();
		}
	
	}


	public void PauseGame() {

		paused = !paused;

		if (paused) {
			UI.SetActive(true);
			Time.timeScale = 0;
		} else {
			UI.SetActive(false);
			Time.timeScale = 1;
		}


	}

	public void RestartGame() {
		Time.timeScale = 1;
		Gameflow gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();
		if (PlayerPrefs.GetInt ("Best Score") < gameflow.CurrentScore) {
			PlayerPrefs.SetInt ("Best Score", gameflow.CurrentScore);
		}
		PlayerPrefs.SetString ("Clear Code", "DEATH");
		PlayerPrefs.Save ();
		UnityEngine.SceneManagement.SceneManager.LoadScene (3);
	}

}
