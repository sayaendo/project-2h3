﻿using UnityEngine;
using System.Collections;

public class DelayedBullet : MonoBehaviour {

	/// <summary>
	/// %
	/// </summary>
	public float slowdown;

	/// <summary>
	/// %
	/// </summary>
	public float acceleration;

	/// <summary>
	/// speed magnitude
	/// </summary>
	public float speedModifier;

	bool started;
	bool accelerating;
	bool noSpeed;

	Vector2 init;

	Rigidbody2D rb;

	void Start () {
		rb = GetComponent<Rigidbody2D> ();
	}

	void FixedUpdate () {

		if (started) {
			if (noSpeed == true) {
				noSpeed = false;
				init = rb.velocity;
			}
			
			if (rb.velocity.magnitude <= 0.3f) {
				accelerating = true;
			}

			if (!accelerating) {
				rb.velocity *= slowdown;
			}

			if (accelerating && rb.velocity.magnitude <= init.magnitude * speedModifier) {
				accelerating = true;
				rb.velocity *= acceleration;
			}

		}
	
	}

	void OnEnable() {
		started = true;
		accelerating = false;
		noSpeed = true;
	}

	void OnDisable() {
		started = false;
	}
}
