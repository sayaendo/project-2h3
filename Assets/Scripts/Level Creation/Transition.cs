﻿using UnityEngine;
using System.Collections;

public class Transition : MonoBehaviour {

	void Start() {
	}

	void Destroy() {
		Destroy (gameObject);
	}

	void Activate() {
		foreach (EnemyAI e in GetComponentsInChildren<EnemyAI> (true)) {
			e.Activate();
		}
	}

	void Deactivate() {
		foreach (EnemyAI e in GetComponentsInChildren<EnemyAI> (true)) {
			e.Deactivate();
		}
	}

}
