﻿using UnityEngine;
using System.Collections.Generic;

public class BossAI : MonoBehaviour, EnemyAI {

	bool activated;

	List<Gun> bufferedGuns;
	List<Gun> nonBufferedGuns;

	/// <summary>
	/// The amount of time it takes to rest its buffered guns between rounds
	/// </summary>
	public int downTime;

	/// <summary>
	/// The amount of time it fires its guns in one round.
	/// </summary>
	public int fireTime;

	/// <summary>
	/// The class of the gun to buffer
	/// Use the empty prefabs in the gun scripts folder
	/// </summary>
	public GameObject bufferedGunClass;

	/// <summary>
	/// The amount of time it takes for the fireTime+downTime cycle to start
	/// </summary>
	public int initialDelay;

	int initialCooldown;


	void Start () {
		Gun[] guns = GetComponents<Gun> ();
		nonBufferedGuns = new List<Gun>();
		bufferedGuns = new List<Gun> ();
		foreach (Gun gun in GetComponents<Gun>()) {
			if (gun.GetType () != bufferedGunClass.GetComponent<Gun>().GetType())
				nonBufferedGuns.Add (gun);
			else
				bufferedGuns.Add (gun);
		}
	}

	int time;
	bool firing = true;

	void FixedUpdate () {
		if (activated) {
			if (initialCooldown < initialDelay) {
				initialCooldown++;
				return;
			}

			if (firing) {
				if (time++ < fireTime) {
					foreach (Gun gun in bufferedGuns) {
						gun.Shoot ();
					}
				} else {
					firing = false;
					time = 0;
				}
			} else {
				if (time++ >= downTime) {
					firing = true;
					time = 0;
				}
			}

			foreach (Gun gun in nonBufferedGuns) {
				gun.Shoot ();
			}
		}
	}

	public void Activate() {
		activated = true;
	}

	public void Deactivate() {
		activated = false;
	}
}
