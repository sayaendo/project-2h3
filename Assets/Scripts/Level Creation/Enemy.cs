﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int life;
    public int points;

    public float powerupDropPercentage;
    public float scoreupDropPercentage;

    public GameObject powerup;
    public GameObject scoreup;
    public GameObject clear;

    public bool notifyOnDeath;

    public GameObject explosion;

    Gameflow gameflow;

    SoundManager sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        gameflow = GameObject.FindGameObjectWithTag("Gameflow").GetComponent<Gameflow>();
    }

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.tag == "ShipBullet") {
			life--;
			other.gameObject.SetActive(false);

			if (life <= 0) {
				Destroy (gameObject);
			} else {
				gameflow.AddScore(points / 100);
			}
		}
	}

	void OnInvisible() {
		//Destroy (gameObject);
	}

	void OnDestroy() {
		if (gameflow.GameOvering)
			return;

		Instantiate (explosion, transform.position, Quaternion.identity);
		sound.PlaySfx(SoundManager.SFX.Explosion);
		if (Ship.Instance.gunCode != 1 && Random.value < powerupDropPercentage) {
			Instantiate(powerup, transform.position, Quaternion.identity);
		} else if (Random.value < scoreupDropPercentage) {
			Instantiate(scoreup, transform.position, Quaternion.identity);
		}

		gameflow.AddScore(points);

		if (notifyOnDeath) {
			Level.Manager.NotifyDeath(transform.position);
		}

		if (clear != null) {
			Instantiate(clear, transform.position, Quaternion.identity);
		}
	}
	

}
