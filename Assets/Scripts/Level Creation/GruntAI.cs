﻿using UnityEngine;
using System.Collections;

public class GruntAI : MonoBehaviour, EnemyAI {

	bool activated;

	Gun[] guns;



	void Start () {
		guns = GetComponents<Gun> ();
	}

	void FixedUpdate () {
	
		if (activated) {
			foreach (Gun gun in guns) {
				gun.Shoot();
			}
		}

	}

	public void Activate() {
		activated = true;
	}

	public void Deactivate() {
		activated = false;
	}

}
