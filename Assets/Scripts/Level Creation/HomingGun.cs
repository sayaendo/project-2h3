﻿using UnityEngine;
using System.Collections;

public class HomingGun : MonoBehaviour, Gun {

	/// <summary>
	/// The initial speed of the bullet when shot from the gun.
	/// </summary>
	public float bulletSpeed;
	
	/// <summary>
	/// The shooting frequency of the gun. Higher is faster, maximum speed is 100.
	/// </summary>
	public int shootingSpeed;
	
	BulletPool bullets;
	
	int frame;
	
	/// <summary>
	/// The direction the bullets will shoot in (the vector is normalized).
	/// </summary>
	Vector2 direction;

	/// <summary>
	/// The location of the gun's nuzzle relative to the gun itself.
	/// </summary>
	public Vector3 offset;

	
	/// <summary>
	/// The tag of the BulletPool that keeps this gun's bullets.
	/// </summary>
	public string bulletType;

	public GameObject explosion;

	public string targetTag;
	
	public string BulletType {
		get
		{
			return bulletType;
		}
		set
		{
			bullets = GameObject.FindWithTag (value).GetComponent<BulletPool> ();
		}
	}

//	GameObject ship;
	
	bool shoot;

    SoundManager sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        bullets = GameObject.FindWithTag (bulletType).GetComponent<BulletPool>();

//		ship = GameObject.FindWithTag ("Player");
	}
	
	void Update() {
		if (frame >= 100) {
			frame = 0;
		}
		
		if (shoot && frame == 0) {
			ShootImmediately();
			shoot = false;
		}
	}
	
	void FixedUpdate() {
		frame += shootingSpeed;
	}
	
	/// <summary>
	/// Tell the gun to shoot. It will shoot the next time it can by its frequency.
	/// </summary>
	public void Shoot() {
		shoot = true;
	}

	public SoundManager.SFX shootSFX;
	
	void ShootImmediately() {
		GameObject target;

		if (targetTag == "Player") {
			target = Ship.Instance.gameObject;
		} else {
			target = GameObject.FindWithTag(targetTag);
		}
		
		if (target != null && target.activeInHierarchy == true) {
			direction = Vector3.ClampMagnitude (Ship.Instance.transform.position - transform.position, 1f);
		} else {
			return;
		}

		GameObject bullet = bullets.GetPooled();
		bullet.transform.position = transform.position + offset;
		bullet.SetActive(true);
		bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

		if (explosion != null) {
			Instantiate (explosion, transform.position, Quaternion.identity);
		}

		sound.PlaySfx(shootSFX);
	}

}
