﻿using UnityEngine;
using System.Collections;

public class DelayedHomingBullet : MonoBehaviour {

	/// <summary>
	/// The de-acceleration speed
	/// </summary>
	public float slowdown;
	
	/// <summary>
	/// The acceleration speed
	/// </summary>
	public float acceleration;
	
	/// <summary>
	/// speed magnitude
	/// </summary>
	public float speedModifier;
	
	bool started;
	bool accelerating;
	bool noSpeed;
	
	Vector2 init;
	
	Rigidbody2D rb;

	public string targetTag;

//	GameObject ship;
	
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
//		ship = GameObject.FindWithTag ("Player");
	}

	void Update() {
	}
	
	void FixedUpdate () {


		
		if (started) {
			if (noSpeed == true) {
				noSpeed = false;
				init = rb.velocity;
			}
			
			if (rb.velocity.magnitude <= 0.3f) {
				accelerating = true;

				GameObject target;

				if (targetTag == "Player") {
					target = Ship.Instance.gameObject;
				} else {
					target = GameObject.FindWithTag(targetTag);
				}
				
				if (target != null && target.activeInHierarchy == true) {
					rb.velocity = Vector3.ClampMagnitude(target.transform.position - transform.position,
					                                     rb.velocity.magnitude);
				}

				if (Ship.Instance.gameObject.activeInHierarchy == true) {

				}

			}
			
			if (!accelerating) {
				rb.velocity *= slowdown;
			}
			
			if (accelerating && rb.velocity.magnitude <= init.magnitude * speedModifier) {
				rb.velocity *= acceleration;
			}
			
		}
		
	}
	
	void OnEnable() {
		started = true;
		accelerating = false;
		noSpeed = true;
	}
	
	void OnDisable() {
		started = false;
	}


}
