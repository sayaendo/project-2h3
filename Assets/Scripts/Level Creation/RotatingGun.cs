﻿using UnityEngine;
using System.Collections;

public class RotatingGun : MonoBehaviour, Gun {
	
	/// <summary>
	/// The initial speed of the bullet when shot from the gun.
	/// </summary>
	public float bulletSpeed;
	
	/// <summary>
	/// The speed at which the gun spins. Higher is faster.
	/// </summary>
	public float rotationSpeed;
	
	/// <summary>
	/// The time it takes for the gun to reverse its rotation (in FixedUpdate time units).
	/// A negative number means the gun will never reverse.
	/// </summary>
	public int rotationCycle;
	
	/// <summary>
	/// The shooting frequency of the gun. Higher is faster, maximum speed is 100.
	/// </summary>
	public int shootingSpeed;
	
	/// <summary>
	/// The angle that the gun's nuzzle points at when created.
	/// </summary>
	public int startingAngle;
	
	public GameObject explosion;
	
	BulletPool bullets;

	public SoundManager.SFX shootSFX;

	/// <summary>
	/// The location of the gun's nuzzle relative to the gun itself.
	/// </summary>
	public Vector3 offset;
	
	/// <summary>
	/// The tag of the BulletPool that keeps this gun's bullets.
	/// </summary>
	public string bulletType;
	
	public string BulletType {
		get
		{
			return bulletType;
		}
		set
		{
			bullets = GameObject.FindWithTag (value).GetComponent<BulletPool> ();
		}
	}
	
	int frame;
	int cycle;
	Vector2 rotator;
	bool shoot;

    SoundManager sound;

	public bool randomize;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        rotator = new Vector2 (0, 1);
		RotateClockwise (startingAngle);
		
		//		bullets = GameObject.FindGameObjectWithTag (bulletType).GetComponent<BulletPool>();
		bullets = GameObject.FindWithTag (bulletType).GetComponent<BulletPool> ();

		prevPos = gameObject.transform.position;
	}
	
	void Update() {
		if (frame >= 100) {
			frame = 0;
		}
		
		if (shoot && frame == 0) {
			ShootImmediately();
			shoot = false;
		}
	}

	Vector3 velocity;
	Vector3 prevPos;
	
	void FixedUpdate() {
		frame += shootingSpeed;

		float rndAngle = (randomize) ? (UnityEngine.Random.Range (-3f, 3f)) : 0;

		RotateClockwise(rotationSpeed + rndAngle);
		
		cycle = (rotationCycle < 0) ? (cycle) : (cycle + 1);
		
		if (cycle == rotationCycle) {
			Reverse ();
			cycle = 0;
		}

		velocity = (gameObject.transform.position - prevPos) * (1 / Time.fixedDeltaTime);
		prevPos = gameObject.transform.position;
	}
	
	void RotateClockwise(float angle) {
		float sin = Mathf.Sin( Mathf.Deg2Rad * angle );
		float cos = Mathf.Cos( Mathf.Deg2Rad * angle );
		
		float tx = rotator.x;
		float ty = rotator.y;
		rotator.x = (cos * tx) - (sin * ty);
		rotator.y = (cos * ty) + (sin * tx);
	}
	
	/// <summary>
	/// Reverses the direction that the gun rotates at.
	/// </summary>
	public void Reverse() {
		rotationSpeed *= -1;
	}
	
	/// <summary>
	/// Tell the gun to shoot. It may not shoot if it can't by its frequency.
	/// </summary>
	public void Shoot() {
		shoot = true;
	}
	
	public void ShootImmediately() {
		GameObject bullet = bullets.GetPooled();
		bullet.transform.position = transform.position  + offset;
		if (explosion != null) {
			Instantiate (explosion, transform.position, Quaternion.identity);
		}
		bullet.SetActive(true);
		bullet.GetComponent<Rigidbody2D>().velocity = rotator * bulletSpeed + 0.1f * new Vector2(velocity.x, velocity.y);

		sound.PlaySfx(shootSFX);
	}
	//
	//	public void ChangeBullet(BulletPool newBullets) {
	//		bullets = newBullets;
	//	}
}