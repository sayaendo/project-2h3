﻿using UnityEngine;
using System.Collections;

public class DirectionalGun : MonoBehaviour, Gun {

	/// <summary>
	/// The initial speed of the bullet when shot from the gun.
	/// </summary>
	public float bulletSpeed;
	
	/// <summary>
	/// The shooting frequency of the gun. Higher is faster, maximum speed is 100.
	/// </summary>
	public int shootingSpeed;
	
	BulletPool bullets;

	public GameObject explosion;
	
	int frame;

	/// <summary>
	/// The direction the bullets will shoot in (the vector is normalized).
	/// </summary>
	public Vector2 direction;

	/// <summary>
	/// The location of the gun's nuzzle relative to the gun itself.
	/// </summary>
	public Vector3 offset;

	/// <summary>
	/// The tag of the BulletPool that keeps this gun's bullets.
	/// </summary>
	public string bulletType;

	public string BulletType {
		get
		{
			return bulletType;
		}
		set
		{
			bullets = GameObject.FindWithTag (value).GetComponent<BulletPool> ();
		}
	}

	bool shoot;

	public SoundManager.SFX shootSFX;

    SoundManager sound;
    
    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();

        bullets = GameObject.FindWithTag (bulletType).GetComponent<BulletPool>();

	}
	
	void Update() {
		if (frame >= 100) {
			frame = 0;
		}
		
		if (shoot && frame == 0) {
			ShootImmediately();
			shoot = false;
		}
	}
	
	void FixedUpdate() {
		frame += shootingSpeed;
	}
	
	/// <summary>
	/// Tell the gun to shoot. It will shoot the next time it can by its frequency.
	/// </summary>
	public void Shoot() {
		shoot = true;
	}

	void ShootImmediately() {
		GameObject bullet = bullets.GetPooled();
		if (explosion != null) {
			Instantiate (explosion, transform.position, Quaternion.identity);
			
		}
		bullet.transform.position = transform.position + offset;
		bullet.SetActive(true);
		bullet.GetComponent<Rigidbody2D>().velocity = direction * bulletSpeed;

		sound.PlaySfx(shootSFX);
	}
}
