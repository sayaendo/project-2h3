﻿using UnityEngine;
using System.Collections;

public class SprayGun : MonoBehaviour, Gun {

	/// <summary>
	/// The initial speed of the bullet when shot from the gun.
	/// </summary>
	public float bulletSpeed;
	
	/// <summary>
	/// The shooting frequency of the gun. Higher is faster, maximum speed is 100.
	/// </summary>
	public int shootingSpeed;
	
	/// <summary>
	/// The angle that the gun's nuzzle point at.
	/// </summary>
	public Vector2 direction;

	/// <summary>
	/// The range (in degrees) that the gun's nuzzle can fluctate between.
	/// </summary>
	public float range;

	BulletPool bullets;

	/// <summary>
	/// The location of the gun's nuzzle relative to the gun itself.
	/// </summary>
	public Vector3 offset;

	public string BulletType {
		get
		{
			return bulletType;
		}
		set
		{
			bullets = GameObject.FindWithTag (value).GetComponent<BulletPool> ();
		}
	}

	/// <summary>
	/// The tag of the BulletPool that keeps this gun's bullets.
	/// </summary>
	public string bulletType;

	Vector2 curDir;
	
	int frame;
	bool shoot;

    SoundManager sound;

    void Start()
    {
        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        //		bullets = GameObject.FindGameObjectWithTag (bulletType).GetComponent<BulletPool>();
        bullets = GameObject.FindWithTag (bulletType).GetComponent<BulletPool> ();
		direction.Normalize ();
		curDir = new Vector2 (direction.x, direction.y);
	}
	
	void Update() {
		if (frame >= 100) {
			frame = 0;
		}

		if (shoot && frame == 0) {
			ShootImmediately();
			shoot = false;
		}
	}
	
	void FixedUpdate() {
		frame += shootingSpeed;

		curDir = new Vector2 (direction.x, direction.y);
		RotateClockwise(Random.Range(-range, range));
	}
	
	void RotateClockwise(float angle) {
		float sin = Mathf.Sin( Mathf.Deg2Rad * angle );
		float cos = Mathf.Cos( Mathf.Deg2Rad * angle );
		
		float tx = curDir.x;
		float ty = curDir.y;
		curDir.x = (cos * tx) - (sin * ty);
		curDir.y = (cos * ty) + (sin * tx);
	}
	
	/// <summary>
	/// Tell the gun to shoot. It will shoot the next time it can by its frequency.
	/// </summary>
	public void Shoot() {
		shoot = true;
	}

	public SoundManager.SFX shootSFX;

	void ShootImmediately() {
		GameObject bullet = bullets.GetPooled();
		bullet.transform.position = transform.position + offset;
		bullet.SetActive(true);
		bullet.GetComponent<Rigidbody2D>().velocity = curDir * bulletSpeed;

		sound.PlaySfx(shootSFX);
	}

}
