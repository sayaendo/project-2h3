﻿using UnityEngine;
using System.Collections;
using System;

public class SpreadGun : MonoBehaviour, Gun {

    /// <summary>
    /// The initial speed of the bullet when shot from the gun.
    /// </summary>
    public float bulletSpeed;

    /// <summary>
    /// The shooting frequency of the gun. Higher is faster, maximum speed is 100.
    /// </summary>
    public int shootingSpeed;

    /// <summary>
    /// The angle that the gun's nuzzle point at.
    /// </summary>
    public Vector2 direction;

    /// <summary>
    /// The range (in degrees) that the gun's nuzzle can fluctate between.
    /// </summary>
    public float range;

    /// <summary>
    /// the amount of bullets fired in each gun fire (spread evenly across the range)
    /// </summary>
    public int density;

    BulletPool bullets;

    /// <summary>
    /// The location of the gun's nuzzle relative to the gun itself.
    /// </summary>
    public Vector3 offset;

	/// <summary>
	/// Whether each spread shooting will be in the exact same place or slightly randomized
	/// </summary>
	public bool randomized;

    public string BulletType
    {
        get
        {
            return bulletType;
        }
        set
        {
            bullets = GameObject.FindWithTag(value).GetComponent<BulletPool>();
        }
    }

    /// <summary>
    /// The tag of the BulletPool that keeps this gun's bullets.
    /// </summary>
    public string bulletType;

    int frame;
    bool shoot;

    SoundManager sound;

	Vector2[] directions;

    // Use this for initialization
    void Start () {

        sound = GameObject.FindGameObjectWithTag("SoundManager").GetComponent<SoundManager>();
        bullets = GameObject.FindWithTag(bulletType).GetComponent<BulletPool>();
        direction.Normalize();

		RotateClockwise (range, ref direction);
		range = 2 * range / (density - 1);
		directions = new Vector2[density];

		for (int i = 0; i < density; i++) {
			directions [i] = new Vector2 (direction.x, direction.y);
			RotateClockwise (-range * i, ref directions [i]);
		}

    }
	
	// Update is called once per frame
	void Update () {

        if (frame >= 100)
        {
            frame = 0;
        }

        if (shoot && frame == 0)
        {
			float range = (randomized) ? UnityEngine.Random.Range (-3f, 3f) : 0;
			foreach (var dir in directions) {
				ShootImmediately (dir, range);
			}
			sound.PlaySfx(shootSFX);
            shoot = false;
        }

    }

    void FixedUpdate()
    {
        frame += shootingSpeed;
    }

    /// <summary>
    /// Tell the gun to shoot. It will shoot the next time it can by its frequency.
    /// </summary>
    public void Shoot()
    {
        shoot = true;
    }

	void RotateClockwise(float angle, ref Vector2 vec) {
		float sin = Mathf.Sin( Mathf.Deg2Rad * angle );
		float cos = Mathf.Cos( Mathf.Deg2Rad * angle );

		float tx = vec.x;
		float ty = vec.y;
		vec.x = (cos * tx) - (sin * ty);
		vec.y = (cos * ty) + (sin * tx);
	}

    public SoundManager.SFX shootSFX;

	void ShootImmediately(Vector2 dir, float range)
    {
		if (randomized)
			RotateClockwise (range, ref dir);

        GameObject bullet = bullets.GetPooled();
        bullet.transform.position = transform.position + offset;
        bullet.SetActive(true);
        bullet.GetComponent<Rigidbody2D>().velocity = dir * bulletSpeed;
		       
    }
}
