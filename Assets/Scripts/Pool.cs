﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pool {

	GameObject pooledObject;
	
	List<GameObject> pool;

	bool canGrow;

	Transform parent;

	public Pool (GameObject pooledObject, int amount) : this (pooledObject, null, amount, false) { }
	
	public Pool(GameObject pooledObject, int amount, bool canGrow) : this (pooledObject, null, amount, canGrow) { }

	public Pool(GameObject pooledObject, Transform parent, int amount, bool canGrow) {
		this.pooledObject = pooledObject;
		this.canGrow = canGrow;
		this.parent = parent;
		
		pool = new List<GameObject>(amount);
		
		for (int i = 0; i < amount; i++) {
			Add ();
		}
	}

	GameObject Add() {
		GameObject obj = GameObject.Instantiate(pooledObject) as GameObject;
		pool.Add(obj);
		obj.SetActive(false);
		obj.transform.parent = parent;
		return obj;
	}

	public GameObject GetPooled() {
		foreach (GameObject obj in pool) {
			if (!obj.activeInHierarchy) {
				return obj;
			}
		}

		if (canGrow) {
			return Add ();
		}

		return null;
	}

	public void Destroy() {
		foreach (GameObject obj in pool) {
			GameObject.Destroy(obj);
		}
	}

}
