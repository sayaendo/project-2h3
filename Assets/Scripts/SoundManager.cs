﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	public enum SFX { PlayerShoot = 0, EnemyShoot = 1, Explosion = 2, Powerup = 3, Pickup = 4, None};
	
	public AudioSource sfx;
	public AudioSource explosion;

	public AudioClip[] clips;

	// Use this for initialization
	void Awake () {
        
	}

	public void PlaySfx(SFX clipIndex) {
		if (clipIndex == SFX.None 
//		    || clipIndex == SFX.Pickup
		    ) {
			return;
		}

//		sfx.clip = clips[(int) clipIndex];
		
		//sfx.pitch = Random.Range(.9f, 1.1f);
//		if ((int)clipIndex == 2 || (int)clipIndex == 3 || (int)clipIndex == 4) {
//			explosion.pitch = Random.Range (.9f, 1.1f);
//			explosion.PlayOneShot (clips [(int)clipIndex]);
////			explosion.clip = clips[(int) clipIndex];
////
////			explosion.Play();
//		} else
//			sfx.Play ();
			sfx.PlayOneShot (clips [(int)clipIndex]);
	}

	// Update is called once per frame
	void Update () {
	
	}
}
