﻿using UnityEngine;
using System.Collections;

public interface Gun {

	void Shoot();

	string BulletType { get; set; }
	 
}
