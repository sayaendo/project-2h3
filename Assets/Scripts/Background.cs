﻿using UnityEngine;
using System.Collections;

public class Background : MonoBehaviour {

	void OnBecameInvisible() {
		transform.position += new Vector3 (0, GetComponent<Renderer>().bounds.extents.y * 4);
	}

}
