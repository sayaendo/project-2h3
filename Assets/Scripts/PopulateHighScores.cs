﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;
using System;
using System.Linq;
using System.Text;

public class PopulateHighScores : MonoBehaviour {

	Text txt;

	public string mode;

	// Use this for initialization
	void Start () {

		Text scoreTxt = GameObject.FindGameObjectWithTag ("LinearShipPool").GetComponent<Text> ();
		Text timeTxt = GameObject.FindGameObjectWithTag ("DelayedShipLgPool").GetComponent<Text> ();

		txt = gameObject.GetComponent<Text> ();

		string mode = PlayerPrefs.GetString ("Mode");

		IList<ScoreEntry> table = new List<ScoreEntry> ();
		for (int i = 0; i < 10; i++) {
			string info = PlayerPrefs.GetString (mode + "HS Info " + i.ToString(), "N/A");
			string time = PlayerPrefs.GetString (mode + "HS Time " + i.ToString(), "N/A");
			int score = PlayerPrefs.GetInt (mode + "HS Score " + i.ToString(), -1);
			table.Add(new ScoreEntry(info, score, time));
		}

		string curInfo = PlayerPrefs.GetString("Clear Code");
		string curTime = DateTime.Now.ToString() + "2";
		int curScore = PlayerPrefs.GetInt("Best Score");

		table.Add(new ScoreEntry(curInfo, curScore, curTime));

		IList<ScoreEntry> sortedTable = table.OrderByDescending (x => x.Score).ToList();

//		StringBuilder bldr = new StringBuilder();
//		bldr.Append (PlayerPrefs.GetString ("Mode") + " MODE:\n");
		scoreTxt.text = "";
		timeTxt.text = "";
		txt.text = PlayerPrefs.GetString ("Mode") + " MODE:\n";
		foreach (ScoreEntry entry in sortedTable) {
			//bldr.Append (entry).Append ("\n");
			txt.text += entry.Info + "\n";
			if (entry.Score == -1) {
				scoreTxt.text += 0 + "\n";
			} else {
				scoreTxt.text += entry.Score + "\n";
			}
			timeTxt.text += entry.Time + "\n";
		}
	
		//txt.text = bldr.ToString();

		for (int i = 0; i < 10; i++) {
			PlayerPrefs.SetString (mode + "HS Info " + i.ToString(), sortedTable[i].Info);
			PlayerPrefs.SetString (mode + "HS Time " + i.ToString(), sortedTable[i].Time);
			PlayerPrefs.SetInt (mode + "HS Score " + i.ToString(), sortedTable[i].Score);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
