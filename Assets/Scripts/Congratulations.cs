﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Congratulations : MonoBehaviour {

	void Start () {

		Text txt = txt = gameObject.GetComponent<Text> ();

		string code = PlayerPrefs.GetString ("Clear Code");
		string mode = PlayerPrefs.GetString ("Mode");
		string reward;

		if (code == "CLEAR" && mode == "NORMAL") {
			reward = "!";
		} else if (mode == "NORMAL") {
			reward = "!!";
		} else if (code == "CLEAR") {
			reward = "!!!";
		} else {
			reward = "!!!!";
		}
			
		txt.text = txt.text + reward;
	
	}

}
